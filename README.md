# DOI mint

DOIs allow data packages to be quickly and easily cited by providing a unique persistent and current identifier of where the data are.
To operate properly, DOIs need to be curated. This requires the involvement of a data manager. The data manager will negotiate with data owners the issuing of DOIs and versioning.
The aim of this project is to create a DOI data manager tool set for the KNB platform.

## DOI tools

The data manager will have tools to mint (create), update and retire DOIs.
The mint tool will create DOIs for data packages and any subsequent versions. The minting will be initiated by the data manager.
The update tool will allow the data manager to review the register of DOIs and edit its fields to keep them current. Updates will be pushed through to the DOI authority.
DOIs cannot be deleted or recycled. The retire tool will allow the data manager to mark a DOI as no longer used. (Q: can a retired DOI be reactivated?)
###Technical background
A machine-to-machine DOI minting service is offered by ANDS. However, this service is not suitable for use by data managers.
Two versions of a user-enabled DOI minting interface are currently available. One is in development by the TERN office, another was written internally. Both versions use ANDS's machine-to-machine service
KNB does not offer native support of DOIs. KNB software cannot communicate directly with DOI minting services nor is it able to accept DOIs as part of data set's description.
EML does not currently support DOIs.Hence, DOI cannot be stored in EML documents. (Is this true?)
Solution design rationale
Since DOIs need to be curated, we believe that it is not appropriate to automatically mint DOIs for records in the KNB platform. Similarly, individual users should not be able to use the DOI tools. Only the data manager is able to mint, manage and retire DOIs.
Because KNB and EML do not support DOI, we need to modify the platform so that it can accept and display DOI for metadata records.
Given the complex architecture of KNB, we will build the necessary DOIs tools with minimal changes to the platform.
Solution workflow and architecture

### The system will have three components:
The data manager's interface: it will allow the data manager to mint, update and retire DOIs
The DOI back end service: it will request from the DOI authority that DOIs be minted, updated or retired
"DOI?" web service: it will return the DOI of any data package on the KNB platform.
These additional tools will also be required:
An email notification application that will send an email to the data manager every time a data package is deposited or updated
An XSL transformation that will display the DOI on the metadata page and include it in the EML/RIF-CS

### Technical Solution

#### Introduction

The DOI Monkey application is a JEE webapp hosted in a Tomcat 6 web server. It is implemented using the Spring Roo rapid application environment, utilising Spring for object composition and web framework, and Hibernate for database persistence. The underlying database is a Postgresql 8.4 database, as used for our Metacat installations.
Although Spring Roo was used in the development of the application, it needn't be used to modify it, and indeed using Spring Roo will undo some manual changes that have been introduced to customise the original implementation created with the Roo framework. Care should be taken therefore when using the Spring Tool Suite, or Spring Roo shell to modify the webapp. If a simple UI change is being made it would be best to use Eclipse without Spring Roo support to avoid unintended changes.
Information Model

#### Database Objects

The application maintains 2 types of objects: DoiRecords and SystemDetails. The DoiRecord contains the metadata needed for interchange with the ANDS DataCite web service, plus some extra required for our Data Managers. The SystemDetails Object stores information about the source Metadata repositories that use our web application for managing DOIs. A DoiRecord has one and only one SystemDetails record associated with it; a SystemDetails record can obviously have zero to many associated DoiRecords.

#### StaticObjects

In addition to these two business objects we also have a statically defined set of User objects, with associated usernames and passwords. These are defined in the file DOIM/src/main/resources/META-INF/spring/applicationContext-security.xml, using a md5 hashcode (see below). To change a users password or add/delete users this file must be edited, redeployed and the webapp restarted for it to take effect.

#### Creating a User

Our application uses the Spring Security 3 framework to manage authentication and authorisation. As mentioned in Static Objects, Users are defined within the Spring Security file DOIM/src/main/resources/META-INF/spring/applicationContext-security.xml, in the authentication-manager element.

```xml
<authentication-manager alias="authenticationManager">
<authentication-provider>
<password-encoder hash="sha-256" />
<user-service>
   <user name="admin"
      password="d42ade8a029a9a6ff096d1d53f68222d75cd36f766b892ed92e94ba7259e6fef"
      authorities="ROLE_USER, ROLE_ADMIN" />
   <user name="alvin"
      password="44ab5cb382c899715fbe29d21d7bfdd4b51023f5388e0835632e86cca431a327"
      authorities="ROLE_USER" />
</user-service>
</authentication-provider>
</authentication-manager>
```

To create password hash codes, use the following command on a Unix/Linux/OSX system.
echo -n your_desired_password | sha256sum

#### Interfaces

The DOI Monkey provides 2 interfaces; a graphical interface to data managers and a machine-to-machine web service to Data Management Systems that manage their DOI minting via DOI Monkey. In addition, the DOI Monkey webapp uses the services of ANDS DataCite (see http://ands.org.au/cite-data/datacite.html). By providing a lookup service for a particular data repository and external ID, external systems can lookup DOIs for display without storing them internally.
Integration with ANDS DataCite

ANDS DataCite is a machine-to-machine web service run by the Australian National Data Service (ANDS). ANDS have purchased a slice of the DOI namespace on behalf of the australian research community and provide the ability to create, update and retire DOIs via a RESTful web service interface. More details on this service can be found at http://ands.org.au/cite-data/datacite.html.
DOI Monkey calls the ANDS web service via the CSIRO java library, which is available in our Nexus artifact repository (see http://180.235.129.194/nexus/content/repositories/external-lib/au/csiro/ands-pid-client/1.0.29/ands-pid-client-1.0.29-sources.jar). At the time of writing only the create and update use cases have been implemented.

The calls to the ANDS web service are made within the DoiRecordController class, during the backend processing of triggered after submitting either the create or update forms.
Integration with Metacat and Bush.fm

Our Data Management systems currently are quite loosely coupled with DOI Monkey. All create, update and retire operations are performed via the DOI Monkey user interface, and our Metacat systems look up DOIs for display with the EML metadata that they maintain via a RESTful web service.
There are two web services provided in DOI Monkey; showBySystemId and showByLocalId.
The showBySystemId Web Service

Calls to this service are used to retrieve all DOIs minted for a particular DMS, and take the form:
```
http://doi.n2o.net.au/doi/doirecords/showBySystemId/7
```
where the 2 at the end of the URL is the 'system ID' within DOI Monkey for the authoritative Data Management system (2 is ASN, 7 is N2O). The current implementation uses server calls to this RESTful web service to render associated DOIs for use in the Metacat RIF-CS feed harvested by the ANDS RDA and TERN portal systems.

#### The showByLocalId Web Service

This web service is called by the Metacat record display pages via AJAX to look up any DOIs that may have been minted in DOI Monkey for a particular local ID and for a particular DMS. It therefore takes two parameters: the system ID and the local record ID. In the following example, the systemID is 7, and the local ID is labadz.16.6. Note also that we must provide a trailing slash in the URL, because of an issue in the web framework used whereby having special characters in the parameters truncates acts as a separator, truncating the passed value.

```
http://doi.n2o.net.au/doi/doirecords/showByLocalId/7/labadz.16.6
```