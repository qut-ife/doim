// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package au.edu.qut.isr.ands.doi.web;

import au.edu.qut.isr.ands.doi.domain.DoiRecord;
import au.edu.qut.isr.ands.doi.domain.SystemDetails;
import au.edu.qut.isr.ands.doi.web.DoiRecordController;
import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import org.joda.time.format.DateTimeFormat;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

privileged aspect DoiRecordController_Roo_Controller {
    
    @RequestMapping(params = "form", produces = "text/html")
    public String DoiRecordController.createForm(Model uiModel) {
        populateEditForm(uiModel, new DoiRecord());
        return "doirecords/create";
    }
    
    @RequestMapping(value = "/{id}", produces = "text/html")
    public String DoiRecordController.show(@PathVariable("id") Long id, Model uiModel) {
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("doirecord", DoiRecord.findDoiRecord(id));
        uiModel.addAttribute("itemId", id);
        return "doirecords/show";
    }
    
    @RequestMapping(produces = "text/html")
    public String DoiRecordController.list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("doirecords", DoiRecord.findDoiRecordEntries(firstResult, sizeNo));
            float nrOfPages = (float) DoiRecord.countDoiRecords() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("doirecords", DoiRecord.findAllDoiRecords());
        }
        addDateTimeFormatPatterns(uiModel);
        return "doirecords/list";
    }
    
    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String DoiRecordController.updateForm(@PathVariable("id") Long id, Model uiModel) {
        populateEditForm(uiModel, DoiRecord.findDoiRecord(id));
        return "doirecords/update";
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String DoiRecordController.delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        DoiRecord doiRecord = DoiRecord.findDoiRecord(id);
        doiRecord.remove();
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/doirecords";
    }
    
    void DoiRecordController.addDateTimeFormatPatterns(Model uiModel) {
        uiModel.addAttribute("doiRecord_datecreated_date_format", DateTimeFormat.patternForStyle("M-", LocaleContextHolder.getLocale()));
        uiModel.addAttribute("doiRecord_dateupdated_date_format", DateTimeFormat.patternForStyle("M-", LocaleContextHolder.getLocale()));
    }
    
    void DoiRecordController.populateEditForm(Model uiModel, DoiRecord doiRecord) {
        uiModel.addAttribute("doiRecord", doiRecord);
        addDateTimeFormatPatterns(uiModel);
        uiModel.addAttribute("systemdetailses", SystemDetails.findAllSystemDetailses());
    }
    
    String DoiRecordController.encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
    
}
