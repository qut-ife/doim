// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package au.edu.qut.isr.ands.doi.domain;

import au.edu.qut.isr.ands.doi.domain.SystemDetails;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

privileged aspect SystemDetails_Roo_Jpa_Entity {
    
    declare @type: SystemDetails: @Entity;
    
    declare @type: SystemDetails: @Table(name = "system_details");
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "system_id")
    private Long SystemDetails.id;
    
    @Version
    @Column(name = "version")
    private Integer SystemDetails.version;
    
    public Long SystemDetails.getId() {
        return this.id;
    }
    
    public void SystemDetails.setId(Long id) {
        this.id = id;
    }
    
    public Integer SystemDetails.getVersion() {
        return this.version;
    }
    
    public void SystemDetails.setVersion(Integer version) {
        this.version = version;
    }
    
}
