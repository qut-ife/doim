package au.edu.qut.isr.ands.doi.domain;


public enum UserRole {

    ROLE_ADMIN, ROLE_USER;
}
