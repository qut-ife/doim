package au.edu.qut.isr.ands.doi.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.httpclient.HttpException;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import au.csiro.doiclient.AndsDoiClient;
import au.csiro.doiclient.AndsDoiResponse;
import au.edu.qut.isr.ands.doi.domain.DoiRecord;

@RequestMapping("/doirecords")
@Controller
@RooWebScaffold(path = "doirecords", formBackingObject = DoiRecord.class)
public class DoiRecordController {

	@Autowired
	private AndsDoiClient doiClient;

	@RequestMapping(method = RequestMethod.POST, produces = "text/html")
	public String create(@Valid DoiRecord doiRecord,
			BindingResult bindingResult, Model uiModel,
			HttpServletRequest httpServletRequest) throws HttpException,
			IOException {
		if (bindingResult.hasErrors()) {
			populateEditForm(uiModel, doiRecord);
			return "doirecords/create";
		}
		uiModel.asMap().clear();

		AndsDoiResponse response = doiClient.mintDOI(doiRecord.getUrl(),
				doiRecord, false);
		if(response.isSuccess()) {
		doiRecord.setIdentifier(response.getDoi());
		doiRecord.setRegistryData(response.getMessage());
		doiRecord.setPublished(true);
		
		doiRecord.persistDoiRecord();
		} else {
			bindingResult.addError(new ObjectError("doiRecord", response.getMessage()));
			populateEditForm(uiModel, doiRecord);
			return "doirecords/create";
		}
		
		return "redirect:/doirecords/"
				+ encodeUrlPathSegment(doiRecord.getId().toString(),
						httpServletRequest);
	}

	@RequestMapping(method = RequestMethod.PUT, produces = "text/html")
	public String update(@Valid DoiRecord doiRecord, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) 
			throws HttpException, IOException {
		
		if (bindingResult.hasErrors()) {
			populateEditForm(uiModel, doiRecord);
			return "doirecords/update";
		}
		uiModel.asMap().clear();

		AndsDoiResponse response = doiClient.updateDOI(doiRecord.getIdentifier(), doiRecord.getUrl(), doiRecord, false);
		if (response.isSuccess()) {
			doiRecord.setRegistryData(response.getMessage());
			doiRecord.setPublished(true);
			doiRecord.merge();
		} else {
			bindingResult.addError(new ObjectError("doiRecord", response
					.getMessage()));
			populateEditForm(uiModel, doiRecord);
			return "doirecords/update";
		}

		return "redirect:/doirecords/"
				+ encodeUrlPathSegment(doiRecord.getId().toString(),
						httpServletRequest);
	}
	
	@RequestMapping(value = "/showByLocalId/{systemId}/{localId}")
	public ModelAndView showByLocalId(@PathVariable String systemId,
			@PathVariable String localId, HttpServletResponse response) {

		DoiRecord doiRecord = new DoiRecord();
		List<DoiRecord> doiRecords = DoiRecord.findDocumentByLocalId(systemId, localId);
		if (doiRecords.size() > 0) {
			doiRecord = doiRecords.get(0);
		} else {
			doiRecord = null;
		}
		
		// render Document object as JSON, then write to HttpServletResponse
		// OutputStream
		try {
			ObjectMapper mapper = new ObjectMapper();
			AnnotationIntrospector introspector = new JaxbAnnotationIntrospector();
			mapper.getDeserializationConfig().setAnnotationIntrospector(
					introspector);
			mapper.getSerializationConfig().setAnnotationIntrospector(
					introspector);
			String json = "";
			if (doiRecord != null) {
				json = mapper.writeValueAsString(doiRecord);
			}
			json = "doi" + "(" + json + ")";
			PrintWriter output = response.getWriter();
			output.write(json);
			response.setContentType("application/javascript");

		} catch (Exception e) {
			// TODO: deal appropriately with each thrown Exception type
		}

		// force response to be served without view
		return null;
	}

	@SuppressWarnings("static-access")
	@RequestMapping(value = "/showBySystemId/{systemId}")
	public ModelAndView showBySystemId(@PathVariable String systemId, HttpServletResponse response) {
		
		List<DoiRecord> doiRecords = DoiRecord.findDocumentsBySystemId(systemId);
		try {
			StringBuilder strBuilder = new StringBuilder("[");
			boolean firstRecord = true;
			for (DoiRecord record : doiRecords) {
				if (!firstRecord) {
					// add comma separator unless first record
					strBuilder.append(",");
				}
				firstRecord=false;
				strBuilder.append("{");
				strBuilder.append("\"localId\":\"").append(record.getLocalId()).append("\",");
				strBuilder.append("\"doi\":\"").append(record.getIdentifier()).append("\"");
				strBuilder.append("}");
			}
			strBuilder.append("]");
			PrintWriter output = response.getWriter();
			output.write(strBuilder.toString());
			response.setContentType("application/javascript");

		} catch (Exception e) {
			// TODO: deal appropriately with each thrown Exception type
		}

		// force response to be served without view
		return null;
	}
}
