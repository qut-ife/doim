package au.edu.qut.isr.ands.doi.web;

import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import au.edu.qut.isr.ands.doi.domain.SystemDetails;

@RequestMapping("/systemdetailses")
@Controller
@RooWebScaffold(path = "systemdetailses", formBackingObject = SystemDetails.class)
public class SystemDetailsController {
}
