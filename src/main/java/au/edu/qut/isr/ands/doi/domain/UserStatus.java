package au.edu.qut.isr.ands.doi.domain;


public enum UserStatus {

    ACTIVE, DORMANT, RESIGNED, TERMINATED;
}
