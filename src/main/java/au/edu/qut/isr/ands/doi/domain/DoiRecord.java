package au.edu.qut.isr.ands.doi.domain;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.transaction.annotation.Transactional;

import au.csiro.doiclient.business.DoiDTO;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(identifierColumn = "doi_record_id", table = "doi_record")
@XmlRootElement
public class DoiRecord extends DoiDTO {

	@Column(length=1024, name = "registry_data", nullable = false)
	private String registryData;

	@Column(name = "published", nullable = false)
	private Boolean published;

	@ManyToOne
	private SystemDetails systemDetails;

	@Column(name = "version", nullable = false)
	private Integer version;

	@Column(name = "user_id", nullable = false)
	private Integer userId;

	@Column(name = "last_updated", nullable = false)
	private Integer lastUpdatedBy;

	@Column(name = "date_created", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date dateCreated;

	@Column(name = "date_updated", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date dateUpdated;

	@Column(name = "identifier", nullable = false)
	private String identifier;

	@NotNull
	@Column
	private String language ="eng";

	@NotNull
	@Column
	private String publicationYear;

	@Column
	@NotNull
	public String subject;

	@Column
	@NotNull
	public String title;

	@Column
	@NotNull
	public String url;

	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column
	@NotNull
	public String publisher;

	@ElementCollection
	public List<String> creators;

	@Column(name = "local_id", nullable = false)
	private String localId;
	
	@Override
	public void setTitle(String title) {
		this.title = title;
		super.setTitle(title);
	}

	@Override
	public void setCreators(List<String> creators) {
		this.creators = creators;
		super.setCreators(creators);
	}

	@Override
	public void setSubject(String subject) {
		this.subject = subject;
		super.setSubject(subject);
	}

	@Override
	public void setLanguage(String language) {
		this.language = language;
		super.setLanguage(language);
	}

	@Override
	public void setPublicationYear(String publicationYear) {
		this.publicationYear = publicationYear;
		super.setPublicationYear(publicationYear);
	}

	@Override
	public void setPublisher(String publisher) {
		this.publisher = publisher;
		super.setPublisher(publisher);
	}

	@Override
	public String getTitle() {
		return this.title;
	}

	@Override
	public List<String> getCreators() {
		return this.creators;
	}

	@Override
	public String getSubject() {
		return this.subject;
	}

	@Override
	public String getLanguage() {
		return this.language;
	}

	@Override
	public String getPublicationYear() {
		return this.publicationYear;
	}

	@Override
	public String getPublisher() {
		return this.publisher;
	}

	@Override
	public String getIdentifier() {
		return this.identifier;
	}

	@Override
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
		super.setIdentifier(identifier);
	}

	public String getLocalId() {
		return localId;
	}

	public void setLocalId(String localId) {
		this.localId = localId;
	}

	@Transactional
	public void persistDoiRecord() {
		if (this.dateCreated == null) {
			this.dateCreated = new Date();
		}
		this.dateUpdated = new Date();
		this.version = 1;
		this.userId = 2;
		this.lastUpdatedBy = 1;
		persist();
	}

	public static List<DoiRecord> findDocumentByLocalId(String systemId, String localId) {
		return entityManager().createQuery("Select d FROM DoiRecord d WHERE d.systemDetails.id = :systemId AND d.localId = :localId", DoiRecord.class).setParameter("systemId", new Long(systemId)).setParameter("localId", localId).getResultList();
	}

	public static List<DoiRecord> findDocumentsBySystemId(String systemId) {
		return entityManager().createQuery("Select d FROM DoiRecord d WHERE d.systemDetails.id = :systemId", DoiRecord.class).setParameter("systemId", new Long(systemId)).getResultList();
    }
}
