package au.edu.qut.isr.ands.doi.domain;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.OneToMany;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(identifierColumn = "system_id", table = "system_details")
public class SystemDetails {

    @Column(name = "name")
    private String name;

    @Column(name = "mode")
    private String mode;

    @Column(name = "base_url")
    private String baseUrl;

    @Column(name = "lookup_url")
    private String lookupUrl;

    @OneToMany(cascade = CascadeType.ALL)
    private Set<DoiRecord> doiRecords = new HashSet<DoiRecord>();
}
